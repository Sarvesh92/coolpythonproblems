# Anagrams are words formed by rearranging the letters of another word
from collections import defaultdict

def group_anagrams(words:list[str])->list[list[str]]:
    """
    Given a list of words, return a list of lists of anagrams
    """
    anagrams_group: defaultdict = defaultdict(list)
    for word in words:
        sorted_word = ''.join(sorted(word))
        anagrams_group[sorted_word].append(word)
    return list(anagrams_group.values())

result:list[list[str]]= group_anagrams(['eat', 'tea', 'tan', 'ate', 'nat', 'bat'])
print(result)