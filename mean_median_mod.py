import math
from operator import le
import sys
from tkinter import SE
from typing import Sequence
from collections import Counter


def get_mean(*, numbers: Sequence[int]) -> int:
    """
    Given a list of numbers, return the mean
    """
    return sum(numbers) / len(numbers)


def get_median(*, numbers: Sequence[int]) -> int:
    """
    Given a list of numbers, return the median
    """
    median = (
        lambda sorted_numbers: sorted_numbers[math.floor(len(sorted_numbers) / 2)]
        if len(sorted_numbers) % 2 != 0
        else (
            (
                sorted_numbers[int((len(sorted_numbers) / 2)) - 1]
                + sorted_numbers[int(len(sorted_numbers) / 2)]
            )
            / 2
        )
    )
    return median(sorted(numbers))


def get_mode(*, numbers: Sequence[int]) -> int:
    """
    Given a list of numbers, return the mode
    """
    return Counter(numbers).most_common()[0][0]


if __name__ == "__main__":
    numbers = [int(num) for num in sys.argv[1:]]
    print(get_mean(numbers=numbers))
    print(get_median(numbers=numbers))
    print(get_mode(numbers=numbers))
